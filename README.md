SteamNoAdmin
============

SteamNoAdmin is a virtual environment created with [Spoon Studio](http://spoon.net/studio) that allows [Steam](http://store.steampowered.com/) to run without administrative privileges.

Building from the command line
------------------------------

SteamNoAdmin can be built from the command line using XStudio

    XStudio SteamNoAdmin.xappl

How to use
----------

Place the compiled SteamNoAdmin.exe executable in the same directory as [Steam](http://store.steampowered.com/) and then use SteamNoAdmin.exe in place of Steam.exe.
